import ahtx0
import utime
import machine
import json
import sys
import io
from wifi import *

LIGHT_SENSOR_MAX = 8192
RAIN_GAUGE_PIN_NUMBER = 12
#in seconds
TIME_BETWEEN_SENDING_READINGS = 4
#microcoapy doesnt seemingly have a way to take advantage of acknowledged CoAP for the user
#this value is changed for viva for sake of time. in original station, its sent 5 times to ensure message is recieved
AMOUNT_TO_REPEAT_SEND = 1

lastRainTick = utime.ticks_ms()
rainGaugeCount = 0


def rainGaugeInterrupt(pin):
    global rainGaugeCount
    global lastRainTick
    #see if theres been a check for rain in the last half second, if so, then its probably switch bounce or smthn non accurate 
    currentTick = utime.ticks_ms()
    timerDif = utime.ticks_diff(currentTick, lastRainTick)
    if timerDif<500:
        print("Switch bounce missed")
    else:
        rainGaugeCount+=1
        print("Valid rain press!")
    lastRainTick = currentTick
    
def main():

    # setup breakout board connections
    i2c = machine.I2C(scl=machine.Pin(9), sda=machine.Pin(8))
    sensor = ahtx0.AHT20(i2c)
    lightSensorPin = machine.ADC(machine.Pin(4))
    rainGaugePin = machine.Pin(RAIN_GAUGE_PIN_NUMBER, machine.Pin.IN)

    # tells rain gauge pin when to trigger and what function to call
    rainGaugePin.irq(trigger=machine.Pin.IRQ_FALLING, handler=rainGaugeInterrupt)

    #setting up wifi using wifi file
    print(connectToWiFi())
    client = microcoapy.Coap()
    client.discardRetransmissions = True
    client.start()
    
    sensorReadings = {}
    global rainGaugeCount
    
    while True:
        try:
            # Brightness is from 0% to 100%
            lightSensorBrightness = lightSensorPin.read() / LIGHT_SENSOR_MAX * 100
            print("Light sensor brightness: ", lightSensorBrightness, "%")
            print("Temperature: ", sensor.temperature,"C")
            print("Humidity: ", sensor.relative_humidity, "%")
            
            sensorReadings['time'] = utime.localtime()
            sensorReadings['light'] = lightSensorBrightness
            sensorReadings['temp'] = sensor.temperature
            sensorReadings['humid'] = sensor.relative_humidity
            sensorReadings['rain'] = rainGaugeCount
            
            for _ in range(AMOUNT_TO_REPEAT_SEND):
                sendPutRequest(client, json.dumps(sensorReadings))
            
            #resets rain gauge count between sending each time
            rainGaugeCount = 0
            
        except BaseException as e:
            print("uh oh")
            error = {}
            error['time'] = utime.localtime()
            #just 'e' is the key of the error, and isnt useful information,but this print_exception thingy contains useful stuff
            error['error'] = exception_to_string(e)
            sendPutRequest(client, json.dumps(error))
        
        finally:
            utime.sleep(TIME_BETWEEN_SENDING_READINGS)
        
def exception_to_string(e, singleLine=True):
  with io.StringIO() as s:
    sys.print_exception(e, s)
    return s.getvalue().replace('\n', ';') if singleLine else s.getvalue()
    
#if __name__ == '__main__':
#    main()

main()
#connectToWiFi()