import aiocoap.resource as resource
import aiocoap
import asyncio
import pandas as pd
import json

currentData = {'time': [], 'humid': [], 'rain': [], 'light': [], 'temp': []}
errors = {'time': [], 'error': []}
fileNumber = [0, 0]

class SensorResource(resource.Resource):
    def __init__(self):
        super().__init__()
        self.state = ""

    async def render_put(self, request):
        #print("PUT payload: %s" % request.payload)
        self.state = request.payload
        print("Updated the sensor resource state: %s " % self.state)
        incomingData(request.payload)

        return aiocoap.Message(code=aiocoap.CHANGED, payload=self.state)

def doServer():
    serverTree = resource.Site()
    serverTree.add_resource(['sensordata', 'receive'], SensorResource())

    asyncio.Task(aiocoap.Context.create_server_context(serverTree, bind=('192.168.1.228', 5683)))

    asyncio.get_event_loop().run_forever()

def incomingData(payload : bytes):

    global currentData
    global errors
    global fileNumber

    messageStr = payload.decode('utf-8')
    message = json.loads(messageStr)

    isSensorReadings = 'light' in message
    if isSensorReadings:

        #packets are sent many times due to lack of acknowledgement accessibility on the micropython coap library, hence:
        if message['time'] not in currentData['time']:
            #if 12 readings or more recieved - for sensor sending every 5 mins, this = 1 hour
            if len(currentData['light']) >= 12:
                df = pd.DataFrame(currentData)
                print("writing sensor data to csv")
                df.to_csv('sensor data hour:' + str(fileNumber[0]) + '.csv')
                fileNumber[0] += 1
                currentData = {'time': [], 'humid': [], 'rain': [], 'light': [], 'temp': []}
            else:
                currentData['time'].append(message['time'])
                currentData['humid'].append(message['humid'])
                currentData['rain'].append(message['rain'])
                currentData['light'].append(message['light'])
                currentData['temp'].append(message['temp'])

    else: #is an error
        # packets are sent many times due to lack of acknowledgement accessibility on the micropython coap library, hence:
        if message['time'] not in errors['time']:
            errors['time'].append(message['time'])
            errors['error'].append(message['error'])
            print("writing error to csv")
            df = pd.DataFrame(errors)
            df.to_csv('error: ' + str(fileNumber[1]) + '.csv')
            fileNumber[1] += 1


if __name__ == '__main__':
    doServer()