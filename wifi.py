import network
import microcoapy
import machine
from secrets import secrets

wlan = network.WLAN(network.STA_IF)
wlan.active(True)

_MY_SSID = secrets['_MY_SSID']
_MY_PASS = secrets['_MY_PASS']
_SERVER_IP = secrets['_SERVER_IP']
_SERVER_PORT = 5683 #default CoAP port
_COAP_PUT_URL = 'sensordata/receive'

#Taken from https://github.com/insighio/microCoAPy/blob/master/examples/esp/esp_wifi_coap_client.py
def connectToWiFi():
    nets = wlan.scan()
    for net in nets:
        ssid = net[0].decode("utf-8")
        if ssid == _MY_SSID:
            print('Network found!')
            wlan.connect(ssid, _MY_PASS)
            while not wlan.isconnected():
                machine.idle()  # save power while waiting
            print('WLAN connection succeeded!')
            break

    return wlan.isconnected()

def sendPutRequest(client, messageContents):
    message = client.put(_SERVER_IP, _SERVER_PORT, _COAP_PUT_URL, messageContents,
                          None, microcoapy.COAP_CONTENT_FORMAT.COAP_TEXT_PLAIN)
    print("[PUT] Message Id: ", message)

    print("Has been recieved: " + str(client.poll(10000)))