import csv
import datetime
import pandas as pd
from influx_prep import getRealTimeOurSensor

fileStart = 'sensor data hour%3A'
fileExtension = '.csv'
numberOfFiles = 59

#a file for changing data to make them useful for gathering data
#seriously inefficient code here, but i dont need to optimise code at this point in the project, would just waste time

"""
done retrospectively, so i call getRealTimeOurSensor
even though the pipeline of data from sensor to influx calls it again

this is one of the spots where i would have done things differently,

in the future, knowing how rain is usually measured, id have prevented this on the sensor station itself
itd be super easy and this function wouldnt be needed
"""
def rainTickToTotalPerDay():
    """
    Reads in rain ticks over time and sums them up to a cumulative value per day for our local server
    :return: None
    """
    data = {'time': [], 'humid': [], 'rain': [], 'light': [], 'temp': [], 'timeDatetime': []}

    for i in range(numberOfFiles):
        with open(fileStart + str(i) + fileExtension, 'r') as file:
            csvFile = csv.reader(file)

            #ignore headers
            next(csvFile, None)
            for lines in csvFile:
                data['time'].append(lines[1])
                data['humid'].append(lines[2])
                data['rain'].append(int(lines[3]))
                data['light'].append(lines[4])
                data['temp'].append(lines[5])
                #make a datetime object from posix time, put that into df for comparison

                ourTimeDatetime = datetime.datetime.fromtimestamp(getRealTimeOurSensor(lines[1]), tz=datetime.timezone.utc)
                data['timeDatetime'].append(ourTimeDatetime)

    df = pd.DataFrame(data)

    #makes the cumsum of rain by date. https://stackoverflow.com/a/51854277 was a big help
    dates = df['timeDatetime'].dt.date
    df['rain_cum_sum'] = df['rain'].groupby(dates).cumsum()

    #cleanup
    df['rain'] = df['rain_cum_sum']
    df = df.drop('rain_cum_sum', axis=1).drop('timeDatetime', axis=1)
    print(df.to_string())
    df.to_csv('myStation_rain_data_comparable.csv')

def rainTipsToMM():
    """
    Reads in rain tips over time and converts them into mm
    :return: None
    """
    data = {'time': [], 'humid': [], 'rain': [], 'light': [], 'temp': []}

    for i in range(numberOfFiles):
        with open('myStation_rain_data_comparable' + fileExtension, 'r') as file:
            csvFile = csv.reader(file)

            #ignore headers
            next(csvFile, None)
            for lines in csvFile:
                data['time'].append(lines[1])
                data['humid'].append(lines[2])
                data['rain'].append(int(lines[3]))
                data['light'].append(lines[4])
                data['temp'].append(lines[5])

    df = pd.DataFrame(data)

    df['rain'] = df['rain'].apply(lambda x: x*0.2794)

    print(df.to_string())
    df.to_csv('myStation_rain_data_comparable_mm.csv')

rainTipsToMM()


