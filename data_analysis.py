import pandas as pd
from influx_prep import getRealTimeOurSensor
import datetime

def daylight_savings_file_finder():
    df = pd.read_csv('data v1\myStation_rain_data_comparable.csv',index_col=0)

    utime_time = df['time']

    actual_time = utime_time.map(getRealTimeOurSensor)
    actual_time2 = actual_time.map(datetime.datetime.fromtimestamp)
    df['time'] = actual_time2

    #prints all data with correct times, manual search for daylight savings in console
    print(df.to_string())

def rain_gauge_diff_finder():

    df = pd.read_csv('data wunderground\wunderground.csv')

    #get the rain (precipitation) column of the data
    rain = df['rain']

    #get all the instances where rain increased from one time to another
    rain = rain.diff()

    #removes all non zero values
    rain = rain.loc[lambda x: x>0]

    #shows us all resulting rain increases
    print(rain)

#calls the datetime.timestamp() method on whats passed into it
def datetime_timestamp_caller(d):
    return int(d.timestamp())

def humidity_stats_differences():
    #i make a new column with the equivalent unixtimes for all datetimes because
    #pandas and datetime comparisons seem weird or deprecated in some documentation
    #changing to unix time just means its simple integer comparisons

    #our station into comparable df formatting
    df_our_station = pd.read_csv('data v1\myStation_rain_data_comparable.csv',index_col=0)
    utime_time = df_our_station['time']
    actual_time_unix = utime_time.map(getRealTimeOurSensor)
    actual_time = actual_time_unix.map(datetime.datetime.fromtimestamp)
    df_our_station['time'] = actual_time
    df_our_station['unixtime'] = actual_time_unix.astype('int64') #its a float otherwise, not useful

    #c3136A station into comparable df formatting
    df_c3136a = pd.read_csv('data comparison station\C6071A.csv')
    df_c3136a_time = df_c3136a['time']
    df_c3136a_time = pd.to_datetime(df_c3136a_time)
    df_c3136a_time_unix = df_c3136a_time.map(datetime_timestamp_caller)
    df_c3136a['time'] = df_c3136a_time
    df_c3136a['unixtime'] = df_c3136a_time_unix

    #wunderground station into comparable df formatting
    df_wunderground = pd.read_csv('data wunderground\wunderground.csv')
    df_wunderground_unixtime = df_wunderground['time']
    df_wunderground_time = df_wunderground_unixtime.map(datetime.datetime.fromtimestamp)
    df_wunderground['time'] = df_wunderground_time
    df_wunderground['unixtime'] = df_wunderground_unixtime

    #filter wunderground and c3136a to be in same date range as our station
    #the highest and lowest data points from our station
    lower_unixtime = int(datetime.datetime(year=2023,month=3,day=24,hour=21,minute=16,second=28).timestamp())
    upper_unixtime = int(datetime.datetime(year=2023,month=3,day=27,hour=9,minute=26,second=52).timestamp())

    #pandas doesnt like it if i do both of these comparisons in one line for some reason
    df_c3136a = df_c3136a[df_c3136a['unixtime'] > lower_unixtime]
    df_c3136a = df_c3136a[df_c3136a['unixtime'] < upper_unixtime]
    df_c3136a.reset_index(drop=True, inplace=True)

    df_wunderground = df_wunderground[df_wunderground['unixtime'] > lower_unixtime]
    df_wunderground = df_wunderground[df_wunderground['unixtime'] < upper_unixtime]
    df_wunderground.reset_index(drop=True, inplace=True)

    
    #point of interest boundries
    lower_poi_unixtime = int(datetime.datetime(year=2023,month=3,day=25,hour=13,minute=0,second=0).timestamp())
    upper_poi_unixtime = int(datetime.datetime(year=2023,month=3,day=26,hour=9,minute=0,second=0).timestamp())

    #inside of points of interest calc
    middle_our_station = df_our_station[df_our_station['unixtime'] > lower_poi_unixtime]
    middle_our_station = middle_our_station[middle_our_station['unixtime'] < upper_poi_unixtime]

    middle_c3136a = df_c3136a[df_c3136a['unixtime'] > lower_poi_unixtime]
    middle_c3136a = middle_c3136a[middle_c3136a['unixtime'] < upper_poi_unixtime]

    middle_wunderground = df_wunderground[df_wunderground['unixtime'] > lower_poi_unixtime]
    middle_wunderground = middle_wunderground[middle_wunderground['unixtime'] < upper_poi_unixtime]

    #finding useful data inside poi
    poi_mean_our_station = middle_our_station['humid'].mean()
    poi_std_dev_our_station = middle_our_station['humid'].std()

    poi_mean_c3136a = middle_c3136a['humid'].mean()
    poi_std_dev_c3136a = middle_c3136a['humid'].std()

    poi_mean_wunderground = middle_wunderground['humid'].mean()
    poi_std_dev_wunderground = middle_wunderground['humid'].std()

    #mean difference from wunderground reference data is the useful metric
    poi_mean_diff_our_station = poi_mean_wunderground - poi_mean_our_station
    poi_mean_diff_c3136a = poi_mean_wunderground - poi_mean_c3136a

    #std_dev diff finder
    poi_std_dev_diff_our_station = poi_std_dev_wunderground - poi_std_dev_our_station
    poi_std_dev_diff_c3136a = poi_std_dev_wunderground - poi_std_dev_c3136a


    
    #outside of points of interest calc
    lower_our_station = df_our_station[df_our_station['unixtime'] < lower_poi_unixtime]
    upper_our_station = df_our_station[df_our_station['unixtime'] > upper_poi_unixtime]

    lower_c3136a = df_c3136a[df_c3136a['unixtime'] < lower_poi_unixtime]
    upper_c3136a = df_c3136a[df_c3136a['unixtime'] > upper_poi_unixtime]

    lower_wunderground = df_wunderground[df_wunderground['unixtime'] < lower_poi_unixtime]
    upper_wunderground = df_wunderground[df_wunderground['unixtime'] < upper_poi_unixtime]

    #finding useful data out of poi

    lower_mean_our_station = lower_our_station['humid'].mean()
    lower_std_dev_our_station = lower_our_station['humid'].std()

    lower_mean_c3136a = lower_c3136a['humid'].mean()
    lower_std_dev_c3136a = lower_c3136a['humid'].std()

    lower_mean_wunderground = lower_wunderground['humid'].mean()
    lower_std_dev_wunderground = lower_wunderground['humid'].std()


    upper_mean_our_station = upper_our_station['humid'].mean()
    upper_std_dev_our_station = upper_our_station['humid'].std()

    upper_mean_c3136a = upper_c3136a['humid'].mean()
    upper_std_dev_c3136a = upper_c3136a['humid'].std()

    upper_mean_wunderground = upper_wunderground['humid'].mean()
    upper_std_dev_wunderground = upper_wunderground['humid'].std()

    #mean difference from wunderground reference data is the useful metric
    #lower
    lower_mean_diff_our_station = lower_mean_wunderground - lower_mean_our_station
    lower_mean_diff_c3136a = lower_mean_wunderground - lower_mean_c3136a

    #std_dev diff finder
    lower_std_dev_diff_our_station = lower_std_dev_wunderground - lower_std_dev_our_station
    lower_std_dev_diff_c3136a = lower_std_dev_wunderground - lower_std_dev_c3136a

    #upper
    upper_mean_diff_our_station = upper_mean_wunderground - upper_mean_our_station
    upper_mean_diff_c3136a = upper_mean_wunderground - upper_mean_c3136a

    #std_dev diff finder
    upper_std_dev_diff_our_station = upper_std_dev_wunderground - upper_std_dev_our_station
    upper_std_dev_diff_c3136a = upper_std_dev_wunderground - upper_std_dev_c3136a

    #actually making the dataframes which show results
    #our_station
    our_station_results_dict = {"Before POI":[abs(lower_std_dev_diff_our_station), abs(lower_mean_diff_our_station)],
                           "POI":[abs(poi_std_dev_diff_our_station), abs(poi_mean_diff_our_station)],
                           "After POI":[abs(upper_std_dev_diff_our_station), abs(upper_mean_diff_our_station)]}

    df_our_station_results = pd.DataFrame(our_station_results_dict)
    df_our_station_results.set_axis(['std_dev_diff_built_station', 'mean_diff_built_station'], axis='index', inplace=True)


    #c3136a
    c3136a_results_dict = {"Before POI":[abs(lower_std_dev_diff_c3136a), abs(lower_mean_diff_c3136a)],
                           "POI":[abs(poi_std_dev_diff_c3136a), abs(poi_mean_diff_c3136a)],
                           "After POI":[abs(upper_std_dev_diff_c3136a), abs(upper_mean_diff_c3136a)]}

    df_c3136a_results = pd.DataFrame(c3136a_results_dict)
    df_c3136a_results.set_axis(['std_dev_diff_c3136a', 'mean_diff_c3136a'], axis='index', inplace=True)


    df = pd.concat([df_our_station_results, df_c3136a_results])
    
    print(df)


humidity_stats_differences()