import datetime

OFFSET = datetime.timedelta(days=8483, hours=20, minutes=22, seconds=47)
MEASUREMENT = 'totalSensorData'

"""
    localtime sent from the sensor starts at first jan 2000, 53 minutes and 41 seconds in
    we need to offset this by a certain amount to get the actual time the data was read

        https://docs.micropython.org/en/v1.8.7/esp8266/library/utime.html

        year includes the century (for example 2014).
        month is 1-12
        mday is 1-31
        hour is 0-23
        minute is 0-59
        second is 0-59
        weekday is 0-6 for Mon-Sun
        yearday is 1-366


    offset is calculated manually by taking the difference between the first data recieved time/date,
    and the date recorded on the sensor and adding an hour
    due to sensor data being sent after one hour of collecting new data

    returns posix time of the actual time our sensor reading was made
    input is a string '[year,month,day,hour,minute,second,weekday,yearday]'
"""
def getRealTimeOurSensor(sensorTime):
    usefulTimeDataString = sensorTime[1:-1]
    usefulTimeData = usefulTimeDataString.split(',')[:-2]
    year, month, day, hour, minute, second = usefulTimeData
    sensorDatetime = datetime.datetime(year=int(year), month=int(month), day=int(day), hour=int(hour), minute=int(minute), second=int(second))
    sensorRealtime = sensorDatetime + OFFSET

    return sensorRealtime.timestamp()

# returns posix time for the C6071A sensor station
# example input is '01/03/2023 00:00'
def getRealTimeC6071A(sensorTime):
    day = int(sensorTime[0:2])
    month = int(sensorTime[3:5])
    year = int(sensorTime[6:10])
    hour = int(sensorTime[11:13])
    minute = int(sensorTime[14:])
    c6071aDatetime = datetime.datetime(year=year, month=month, day=day, hour=hour, minute=minute)

    return c6071aDatetime.timestamp()

#makes an influx point from a message got from my server station

def stationInfluxPoint(message: dict, stationName):

    humidityFieldSet = 'humidity='+str(message['humid'])
    tempFieldSet = 'temperature='+str(message['temp'])

    # default time accuracy for timestamps in influx is nanoseconds,
    # im sure theres a way to change this in some object somewhere, but its not easy to find...

    """
    this is another thing id do differently had i had time
    having a new block and new get time function for every new tag i wanna input
    just isnt effective code, theres alot of code duplication here
    but its neccesary due to lack of foresight and manual calling of inputting data
    ideally id pass code through data_corrections.py and have a standard way of storing data
    immediately as it comes in, but thats not what i did due to storing data from one place,
    moving onto the next which has another way of storing data etc etc
    
    """

    if stationName == 'myStation':
        tagSet='station=myStation'

        lightFieldSet = 'light=' + str(message['light'])
        rainFieldSet = 'rain=' + str(message['rain'])

        timeInNanoseconds = str(int(getRealTimeOurSensor(message['time']) * 1000000000))
        p = f'{MEASUREMENT},{tagSet} {humidityFieldSet},{rainFieldSet},{lightFieldSet},{tempFieldSet} {timeInNanoseconds}'
    elif stationName == 'C6071A':
        tagSet = 'station=C6071A'

        # no light readings from this station
        rainFieldSet = 'rain=' + str(message['rain'])

        timeInNanoseconds = str(int(getRealTimeC6071A(message['time']) * 1000000000))
        p = f'{MEASUREMENT},{tagSet} {humidityFieldSet},{rainFieldSet},{tempFieldSet} {timeInNanoseconds}'
    elif stationName == 'wunderground':
        tagSet = 'station=wunderground'

        #no light readings  readings :c
        rainFieldSet = 'rain=' + str(message['rain'])

        #time returned by wunderground is already unix time, so we just need to convert to nanoseconds for our uses...
        timeInNanoseconds = str(int(message['time']) * 1000000000)
        p = f'{MEASUREMENT},{tagSet} {humidityFieldSet},{rainFieldSet},{tempFieldSet} {timeInNanoseconds}'

    elif stationName == 'myStation_rain_data_comparable':
        tagSet='station=myStation_rain_data_comparable'

        lightFieldSet = 'light=' + str(message['light'])
        rainFieldSet = 'rain=' + str(message['rain'])

        timeInNanoseconds = str(int(getRealTimeOurSensor(message['time']) * 1000000000))
        p = f'{MEASUREMENT},{tagSet} {humidityFieldSet},{rainFieldSet},{lightFieldSet},{tempFieldSet} {timeInNanoseconds}'

    elif stationName == 'myStation_rain_data_comparable_mm':
        tagSet='station=myStation_rain_data_comparable_mm'

        lightFieldSet = 'light=' + str(message['light'])
        rainFieldSet = 'rain=' + str(message['rain'])

        timeInNanoseconds = str(int(getRealTimeOurSensor(message['time']) * 1000000000))
        p = f'{MEASUREMENT},{tagSet} {humidityFieldSet},{rainFieldSet},{lightFieldSet},{tempFieldSet} {timeInNanoseconds}'

    return p


