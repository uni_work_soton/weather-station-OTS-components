import csv
import requests
from influxsecrets import secrets
import json

wundergroundAPIKey = secrets['wundergroundAPIKey']
stationID = 'ISOUTH905'

#format the csv file for midRangeData in a way to be uploaded to influx
#time is first column, temp is third, humidity tenth, rain is fifteenth
def C6071ADataFormat(data):
    dataSplit = data[0].split(',')
    #some data entries are simply timecodes without data, so we filter these
    if dataSplit[1] == '':
        return False

    """
    from csv_to_influx.py:
    message = {'time': lines[0],
               'humid': lines[1],
               'rain': lines[2],
               'temp': lines[3]}
    so we make csv in this column order
    """

    row = [dataSplit[0], dataSplit[9], dataSplit[14], dataSplit[2]]
    return row

#takes the original csv file and turns it into one usable for our current functions for turning csv -> influxdb
def C6071ADataLoad():
    with open('C6071A_original.csv', mode='rt') as originalFile:
        csvOriginalFile = csv.reader(originalFile, delimiter = ';')

        # ignore headers
        next(csvOriginalFile, None)

        header = ['time', 'humid', 'rain', 'temp']

        with open('C6071A.csv', 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(header)

            for row in csvOriginalFile:
                newRow = C6071ADataFormat(row)
                #some rows need to be filtered out
                if newRow:
                    writer.writerow(newRow)

#takes in one single recording and turns it into the csv format we can use
def wundergroundDataFormat(data):
    """
    from csv_to_influx.py
    message = {'time': lines[0],
        'humid': lines[1],
        'temp': lines[2],
        'rain': lines[3]
        }
    """
    time = data['epoch']
    humidity = data['humidityAvg']
    temperature = data['metric']['tempAvg']
    rain = data['metric']['precipTotal']
    row = [time, humidity, temperature, rain]
    return row

#for station v1 takes in historical data from wunderground api and
def wundergroundDataLoad():
    response24 = requests.get(
        'https://api.weather.com/v2/pws/history/all?stationId='+stationID+'&format=json&units=m&numericPrecision=decimal&date=20230324&apiKey='+wundergroundAPIKey)
    response25 = requests.get(
        'https://api.weather.com/v2/pws/history/all?stationId=' + stationID + '&format=json&units=m&numericPrecision=decimal&date=20230325&apiKey=' + wundergroundAPIKey)
    response26 = requests.get(
        'https://api.weather.com/v2/pws/history/all?stationId=' + stationID + '&format=json&units=m&numericPrecision=decimal&date=20230326&apiKey=' + wundergroundAPIKey)
    response27 = requests.get(
        'https://api.weather.com/v2/pws/history/all?stationId=' + stationID + '&format=json&units=m&numericPrecision=decimal&date=20230327&apiKey=' + wundergroundAPIKey)

    allResponses = [response24, response25, response26, response27]

    #get the list of all readings from all four days we measured data from for station v1
    observations = [json.loads(i.content)['observations'] for i in allResponses]
    flatten_observations = [item for sublist in observations for item in sublist]

    #no light level readings available online :c
    header = ['time', 'humid', 'temp', 'rain']

    with open('wunderground.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(header)

        for datapoint in flatten_observations:
            writer.writerow(wundergroundDataFormat(datapoint))


wundergroundDataLoad()