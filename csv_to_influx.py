import influx_prep
from influxdb_client import InfluxDBClient
from influxdb_client.client.write_api import SYNCHRONOUS
from influxsecrets import secrets
import csv

__BUCKET = secrets['bucket']
__ORG = secrets['org']
__TOKEN = secrets['token']

# influx db local host
url = "torrent-cube.local:8086"

client = InfluxDBClient(org=__ORG, token=__TOKEN, url=url)
writeAPI = client.write_api(write_options=SYNCHRONOUS)


def writeCSVMyStationToInflux(csvName, stationName='myStation'):
    # get points for all rows in csv
    with open(csvName, mode='r') as file:
        csvFile = csv.reader(file)

        # ignore headers
        next(csvFile, None)
        for lines in csvFile:
            message = {'time': lines[1],
                       'humid': lines[2],
                       'rain': lines[3],
                       'light': lines[4],
                       'temp': lines[5]}
            p = influx_prep.stationInfluxPoint(message, stationName)
            writeAPI.write(__BUCKET, __ORG, p)


def writeCSVC6071AToInflux():
    # get points for all rows in csv
    with open('C6071A.csv', mode='rt') as file:
        csvFile = csv.reader(file)

        # ignore headers
        next(csvFile, None)
        for lines in csvFile:
            message = {'time': lines[0],
                       'humid': lines[1],
                       'rain': lines[2],
                       'temp': lines[3]}
            p = influx_prep.stationInfluxPoint(message, 'C6071A')
            writeAPI.write(__BUCKET, __ORG, p)


def writeCSVWundergroundToInflux():
    # get points for all rows in csv
    with open('wunderground.csv', mode='rt') as file:
        csvFile = csv.reader(file)

        # ignore headers
        next(csvFile, None)
        for lines in csvFile:
            message = {'time': lines[0],
                       'humid': lines[1],
                       'temp': lines[2],
                       'rain': lines[3]
                       }
            p = influx_prep.stationInfluxPoint(message, 'wunderground')
            writeAPI.write(__BUCKET, __ORG, p)

"""
#populating influxdb
for i in range(1,59):
    writeCSVMyStationToInflux('sensor data hour%3A'+str(i)+'.csv')
"""
"""
#populating influx with C6071A data
writeCSVC6071AToInflux()
"""

# populating influx with wunderground data
writeCSVMyStationToInflux('myStation_rain_data_comparable_mm.csv', 'myStation_rain_data_comparable_mm')

