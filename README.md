# Weather Station OTS Components

A weather station made from industry-capable sensors available "Off the shelf" (OTS). It collected data until the battery ran out of charge, upon collecting the data, I performed data analysis and graphed it via Grafana to compare the sensors against other data sources.

This repo is a merge of two repos used during development, the sensor-station, and the sensor-server.

The sensor-station core was a feather-s2 microcontroller running micropython, connected with an AHT20 temperature and humidity sensor as well as a non-branded rain gauge my project supervisor had to hand:

![sensor-station](sensor-station.png)

Data was collected every 5 minutes and sent to a server I owned via CoAP, where it was collected for later use.

The server ran python scripts which manipulated, joined and uploaded the data into an influxDB database. The data was finally viewable via Grafana which led to some interesting outcomes to be seen. Such as discovering periods of innaccuracy between the station and other data sources:

![station-POI](period of interest graph.png)

The project was a success, it showed a proof of concept for such a future system to be viable IF some problem variables such as power usage could be addressed, as was discussed in the final report.

This was my dissertation project for graduation from university. 
